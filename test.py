import requests
from time import sleep

s = 5 # sleep seconds

print("TEST OPENING")
print("===========================================================")
print("\nThis test shows all of rest api process"
      "\nIf you want to see data, go to MongoDB Atlas environment\n")
print("===========================================================")
print("\nFirst of all creating a user")
print("\ncreating...")
sleep(s)

# register
url = "http://localhost:5000/register"
data = {'user': 'atlas', 'passw': 'paris'}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post(url, json=data, headers=headers)
print(r.text)
print("\nusername that {r.text} created\n")
print("===========================================================")
sleep(s)
print("\nLogin with created user")

# login
url = "http://localhost:5000/login"
data = {'user': 'atlas', 'passw': 'paris'}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post(url, json=data, headers=headers)
print("\nChecking username and password")
sleep(s)
print(r.text)
print(f"\nYes!!! succesfully login\n")
print("===========================================================")
sleep(s)
print("\nLet go create a property")

# create property
url = "http://localhost:5000/property/create"
property = {  "name": "fransa",
              "numberOfBedrooms": 15,
              "numberOfRooms": 28}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
e = requests.post(url, json=property, headers=headers)
print("\nChecking user is there and property attributes")
sleep(s)
# print(e.text)
print("\nSuper everybody clap\n")
print("===========================================================")

sleep(s)
print("\nLet's look at the created property")
# get proporty
url = "http://localhost:5000/property"
data = {'name': 'fransa'}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post(url, json=data, headers=headers)
print("\nSearhin proporties with name of property")
print("\nResult coming...")
sleep(s)
print(r.text)


# delete proporty
url = "http://localhost:5000/property/delete"
data = {'property_id': 1574634706585734}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post(url, json=data, headers=headers)
print(r.text)


